
//Earth’s radius, sphere
var R=6378137;
function offset(lat, lon, dn, de) {
    //Coordinate offsets in radians
    var dLat = dn/R,
        dLon = de/(R*Math.cos(Math.PI*lat/180));

    //OffsetPosition, decimal degrees
    return {
        latitude: lat + dLat * 180/Math.PI,
        longitude: lon + dLon * 180/Math.PI
    }
}
function distance(lat1, lon1, lat2, lon2) {
    var radlat1 = Math.PI * lat1/180,
        radlat2 = Math.PI * lat2/180,
        radlon1 = Math.PI * lon1/180,
        radlon2 = Math.PI * lon2/180,
        theta = lon1-lon2,
        radtheta = Math.PI * theta/180,
        dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
    dist = Math.acos(dist);
    dist = dist * 180/Math.PI;
    dist = dist * 60 * 1.1515;
    
    // in km
    dist = dist * 1609.344;
    return dist;
}

function locationQueryField(fieldName, query, ctx) {
    // get location filter from the query
    var locationQuery = query[fieldName];

    if(!(locationQuery && typeof locationQuery.longitude === 'number' && typeof locationQuery.latitude === 'number' && typeof locationQuery.radius === 'number')) {
        console.warn('cannot run location query, wrong data! %j', locationQuery);
        if(ctx && process.env.ENV === 'development') ctx.res.setHeader('X-EXCOL-LocationQuery', 'Wrong args:' + JSON.stringify(locationQuery));

        return;
    }

    // sanitize
    locationQuery.radius = Math.max(1, Math.min(20000000, locationQuery.radius));

    if(ctx) ctx.res.setHeader('X-EXCOL-LocationQuery', JSON.stringify(locationQuery));
    delete query[fieldName];

    // calculate square with length = radius*2
    var tmp1 = offset(locationQuery.latitude, locationQuery.longitude, locationQuery.radius, locationQuery.radius),
        tmp2 = offset(locationQuery.latitude, locationQuery.longitude, -locationQuery.radius, -locationQuery.radius),
        latitudeMin = Math.min(tmp1.latitude, tmp2.latitude),
        latitudeMax = Math.max(tmp1.latitude, tmp2.latitude),
        longitudeMin = Math.min(tmp1.longitude, tmp2.longitude),
        longitudeMax = Math.max(tmp1.longitude, tmp2.longitude);

    query[fieldName+'Longitude'] = {'$gte': longitudeMin, '$lte': longitudeMax};
    query[fieldName+'Latitude'] = {'$gte': latitudeMin, '$lte': latitudeMax};

    // until there's a better way
    query._getLocationQuery = function() {
        return locationQuery;
    }
};

function locationAddDistanceField(fieldName, query, data, ctx) {
    var locationQuery = query._getLocationQuery && query._getLocationQuery();
    if(!locationQuery) return;
    console.log('locationAddDistanceField: ', data, query, locationQuery);

    // need to go backwards, as we'll change the array
    for(var i = data.length, itm; i--;) {
        itm = data[i];
        if(itm[fieldName+'Latitude'] && itm[fieldName+'Longitude']) {
            itm[fieldName + 'DistanceToQuery'] = distance(locationQuery.latitude, locationQuery.longitude, itm[fieldName+'Latitude'], itm[fieldName+'Longitude'])
        }
        if(itm[fieldName + 'DistanceToQuery'] && itm[fieldName + 'DistanceToQuery'] > locationQuery.radius) {
            data.splice(i, 1);
        }
    }
};

(function(ExtendedCollection) {
    var _runEvent = ExtendedCollection.prototype.runEvent;

    ExtendedCollection.prototype.runEvent = function(ev, ctx, domain, done) {
        domain.locationQueryField = locationQueryField.bind(this);
        domain.locationAddDistanceField = locationAddDistanceField.bind(this);
        return _runEvent.apply(this, arguments);
    };
})(require('../dpd-extended-collection/index'));