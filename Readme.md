## dpd-extended-collection-geolocation - Geolocation query functions for deployd extended-Collections

TBD

### Requirements

* deployd (you'd have guessed that, probably :-))
* dpd-extended-collection
* Any Collection with some (or all) of these custom fields:
TBD

### Installation

In your app's root directory, type `npm install dpd-extended-collection-geolocation` into the command line or [download the source](https://bitbucket.org/simpletechs/dpd-extended-collection-geolocation). This should create a `dpd-extended-collection-geolocation` directory in your app's `node_modules` directory.

See [Installing Modules](http://docs.deployd.com/docs/using-modules/installing-modules.md) for details.

### Setup

TBD
No additional setup is required, as every `ExtendedCollection` (and every `Resource` that inherits from `ExtendedCollection`) is automatically extended.

### Usage

TBD

### Credits

`dpd-extended-collection-geolocation` is the work of [simpleTechs.net](https://www.simpletechs.net)